#ifndef _ERL_NIF_H
#define _ERL_NIF_H

#include <stdint.h>
#include <stddef.h>
#include <stdbool.h>

typedef struct ErlMap ErlMap;
typedef struct ErlRef ErlRef;
typedef struct ErlPid ErlPid;
typedef struct ErlFun ErlFun;
typedef struct ErlPort ErlPort;
typedef struct ErlProc ErlProc;
typedef struct ErlModule ErlModule;

typedef union {
    double f64;
    int32_t i32;
    uint32_t u32;
    uint64_t u64;
    uintptr_t ptr;
    uint16_t words[4];
} ErlTerm;

typedef struct {
    size_t length;
    ErlTerm* items;
} ErlTuple;

typedef struct {
    size_t length;
    uint8_t* bytes;
} ErlBinary;

typedef struct {
    uint8_t* name;
    uint8_t length;
    uint16_t ordinal;
} ErlAtom;


#endif // _LZR_NIF_H