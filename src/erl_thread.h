#ifndef _ERL_THREAD_H
#define _ERL_THREAD_H

#include "erl_sys.h"
typedef void* (*ErlThreadFunc) (void* argv);

#ifdef ERL_WINDOWS
    #include <Windows.h>
    typedef HANDLE ErlThread;
    typedef SRWLOCK ErlRwLock;
    typedef CRITICAL_SECTION ErlMutex;
    typedef CONDITION_VARIABLE ErlCondVar;

    // condition variable interface
    #define erl_condvar_free(cv)
    #define erl_condvar_init(cv) InitializeConditionVariable(cv)
    #define erl_condvar_signal(cv) WakeConditionVariable(cv)
    #define erl_condvar_wait(cv, mutex) SleepConditionVariableCS(cv, mutex, INFINITE)

    // mutex interface
    #define erl_mutex_free(mutex)
    #define erl_mutex_init(mutex) InitializeCriticalSection(mutex)
    #define erl_mutex_lock(mutex) EnterCriticalSection(mutex)
    #define erl_mutex_unlock(mutex) LeaveCriticalSection(mutex)

    // read-write lock interface
    #define erl_rwlock_free(rw)
    #define erl_rwlock_init(rw) InitializeSRWLock(rw)
    #define erl_rwlock_rlock(rw) AcquireSRWLockShared(rw)
    #define erl_rwlock_wlock(rw) AcquireSRWLockExclusive(rw)
    #define erl_rwlock_runlock(rw) ReleaseSRWLockShared(rw)
    #define erl_rwlock_wunlock(rw) ReleaseSRWLockExclusive(rw)
    #define erl_rwlock_tryrlock(rw) TryAcquireSRWLockShared(rw)
    #define erl_rwlock_trywlock(rw) TryAcquireSRWLockExclusive(rw)

    // thread interface
    #define erl_thread_exit() ExitThread(NULL)
    #define erl_thread_yield() SwitchToThread()
    #define ERL_THREAD_FUNC(name, argv) DWORD WINAPI name(PVOID argv)

    ERL_FORCE_INLINE void erl_thread_join(ErlThread thread) {
        WaitForSingleObject(thread, INFINITE);
        CloseHandle(thread);
    }

    ERL_FORCE_INLINE ErlThread erl_thread_init(ErlThreadFunc func, void* argv) {
        DWORD thread_id;
        return CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)func, (LPVOID) argv, 0, &thread_id);
    }

    ERL_FORCE_INLINE int erl_thread_cores() {
        SYSTEM_INFO sys_info;
        GetSystemInfo(&sys_info);
        return (int) sys_info.dwNumberOfProcessors;
    }

#else
    #include <sched.h>
    #include <pthread.h>
    #include <sys/sysinfo.h>
    typedef pthread_t ErlThread;
    typedef pthread_mutex_t ErlMutex;
    typedef pthread_cond_t ErlCondVar;
    typedef pthread_rwlock_t ErlRwLock;

    // condition variable interface
    #define erl_condvar_free(cv) pthread_cond_destroy(cv)
    #define erl_condvar_init(cv) pthread_cond_init(cv, NULL)
    #define erl_condvar_signal(cv) pthread_cond_signal(cv)
    #define erl_condvar_wait(cv, mutex) pthread_cond_wait(cv, mutex)

    // mutex interface
    #define erl_mutex_free(mutex) pthread_mutex_destroy(mutex)
    #define erl_mutex_init(mutex) pthread_mutex_init(mutex)
    #define erl_mutex_lock(mutex) pthread_mutex_lock(mutex)
    #define erl_mutex_trylock(mutex) pthread_mutex_trylock(mutex)
    #define erl_mutex_unlock(mutex) pthread_mutex_unlock(mutex)

    // read-write lock interface
    #define erl_rwlock_free(rw) pthread_rwlock_destroy(rw)
    #define erl_rwlock_init(rw) pthread_rwlock_init(rw)
    #define erl_rwlock_rlock(rw) pthread_rwlock_rdlock(rw)
    #define erl_rwlock_wlock(rw) pthread_rwlock_wrlock(rw)
    #define erl_rwlock_runlock(rw) pthread_rwlock_unlock(rw)
    #define erl_rwlock_wunlock(rw) pthread_rwlock_unlock(rw)
    #define erl_rwlock_tryrlock(rw) pthread_rwlock_tryrdlock(rw)
    #define erl_rwlock_trywlock(rw) pthread_rwlock_trywrlock(rw)

    // thread interface
    #define erl_thread_cores() get_nprocs()
    #define erl_thread_yield() sched_yield()
    #define erl_thread_exit() pthread_exit(NULL)
    #define erl_thread_join(thread) pthread_join(thread, NULL)
    #define ERL_THREAD_FUNC(name, argv) void* name(void* argv)

    ERL_FORCE_INLINE ErlThread erl_thread_init(ErlThreadFunc func, void* argv) {
        ErlThread thread;
        pthread_create(&thread, NULL, func, argv);
        return thread;
    }
    

#endif

#endif // _ERL_THREAD_H