#ifndef _ERL_ATOMIC_H
#define _ERL_ATOMIC_H

#include <stdbool.h>
#include <stdatomic.h>

#define ERL_ATOMIC(T) volatile T
#define ERL_ALIGN(N) __attribute__((aligned(N)))

#define ERL_ATOMIC_RELAXED __ATOMIC_RELAXED
#define ERL_ATOMIC_CONSUME __ATOMIC_CONSUME
#define ERL_ATOMIC_ACQUIRE __ATOMIC_ACQUIRE
#define ERL_ATOMIC_RELEASE __ATOMIC_RELEASE
#define ERL_ATOMIC_ACQ_REL __ATOMIC_ACQ_REL
#define ERL_ATOMIC_SEQ_CST __ATOMIC_SEQ_CST

#define erl_atomic_fence(memory_order) \
    __atomic_thread_fence(memory_order)

#define erl_atomic_load(ptr, memory_order) \
    __atomic_load_n(ptr, memory_order)

#define erl_atomic_store(ptr, value, memory_order) \
    __atomic_store_n(ptr, value, memory_order)

#define erl_atomic_add(ptr, value, memory_order) \
    __atomic_fetch_add(ptr, value, memory_order)

#define erl_atomic_sub(ptr, value, memory_order) \
    __atomic_fetch_sub(ptr, value, memory_order)

#define erl_atomic_swap(ptr, value, memory_order) \
    __atomic_exchange_n(ptr, value, memory_order)

#define erl_atomic_cas_weak(ptr, expect, swap, memory_order) \
    __atomic_compare_exchange_n(ptr, expect, swap, true, memory_order, memory_order)
    
#define erl_atomic_cas_strong(ptr, expect, swap, memory_order) \
    __atomic_compare_exchange_n(ptr, expect, swap, false, memory_order, memory_order)

#endif // _ERL_ATOMIC_H