#ifndef _ERL_SYS_H
#define _ERL_SYS_H

#include "erl_nif.h"

#if defined(_WIN32)
    #define ERL_WINDOWS
#endif
#if defined(__x86__) || defined(__x86_64__)
    #define ERL_x86
#endif
#if defined(__x86_64__) || defined(__LP64__)
    #define ERL_64
#endif

#define ERL_THREAD_LOCAL __thread
#define ERL_LIKELY(expr) __builtin_expect(!!(expr), 1)
#define ERL_UNLIKELY(expr) __builtin_expect(!!(expr), 0)
#define ERL_FORCE_INLINE inline __attribute__((__always_inline__))

#endif // _ERL_SYS_H